﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using workingDayButton.Models;


namespace workingDayButton
{
    public class ButtonController : Controller
    {

        // GET: /<controller>
        public IActionResult Index()
        {
            //set variables to pass into model class
            var timeNow = DateTime.Now;
            var dayToday = DateTime.Now.AddDays(1.0).DayOfWeek.ToString();

            //if the weekend return monday for next working day 
            if (dayToday == DayOfWeek.Saturday.ToString() || dayToday == DayOfWeek.Sunday.ToString())
            {
                dayToday = DayOfWeek.Monday.ToString();
            }

            ////As my logic using a +1 add day I cound the weekend as Friday/Saturday 
            //else {
            //    var nextDay = timeNow.AddDays(2);
            //    dayToday = nextDay.DayOfWeek.ToString();
            //}

            //setup an instance of the button model 
            TimeNow userTime = new TimeNow
            {
                whatDay = timeNow.DayOfWeek.ToString(),
                workingDay = dayToday
            };

            //pass data in through view data 
            ViewData["time"] = userTime;

            return View();
        }
    }
}
